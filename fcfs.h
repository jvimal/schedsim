
struct FCFSScheduler : public Scheduler {
	list<Job*> q;
	double capacity;
	SchedEvent *e;

	FCFSScheduler(double capacity):capacity(capacity) {
		name = "FCFS";
	}

	void addjob(Job *j) {
		q.push_back(j);
		if (!e->inq)
			sim->schedule(e, 0);
	}

	int qlen() {
		return q.size();
	}

	void start() {
		e = new SchedEvent(this);
		e->sim = sim;
		e->start();
	}

	void clean_finished() {
		if (q.empty())
			return;

		Job *j = q.front();
		if (j->rem == 0) {
			q.pop_front();
			Scheduler::finish(j);
			delete j;
		}
	}

	/* Process jobs and return elapsed time */
	double process() {
		clean_finished();

		if (!q.empty()) {
			Job *j = q.front();
			j->rem -= j->size;
			return j->size / capacity;
		}

		return 0;
	}
};
