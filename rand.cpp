#include <cassert>
#include <time.h>
using namespace std;

#include <stdio.h>

#include "random.h"

int main() {
	rand_init();
	double mean_size = 10.0;
	RandomPareto *sizes = new RandomPareto(mean_size, 1.1);

	for (int i = 0; i < 100; i++) {
		printf("%.3f\n", sizes->gen());
	}

	return 0;
}
