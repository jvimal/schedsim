#define MAX_LEVELS 4

struct MLPSScheduler : public Scheduler {
	list<Job*> q[MAX_LEVELS];
	SchedEvent *e;
	list<Job*>::iterator first;
	int levels, fcfs_level;
	int level_size_cutoffs[MAX_LEVELS];

	MLPSScheduler(double capacity, int levels, int fcfs_level):levels(levels), fcfs_level(fcfs_level) {
		char buff[32];
		this->capacity = capacity;
		sprintf(buff, "MLPS-%d", levels);
		name = buff;
	}

	void set_cutoff(int level, int start_size) {
		level_size_cutoffs[level] = start_size;
	}

	void addjob(Job *j) {
		int l = 0;
		for (int i = 0; i < levels; i++) {
			if (j->size > level_size_cutoffs[i])
				l = i;
		}

		assert(l < levels);
		q[l].push_back(j);
		if (!e->inq) {
			last_process = sim->now;
			sim->schedule(e, 0);
		} else {
			process();
		}
	}

	int qlen() {
		int ret = 0;
		for(int i = 0; i < levels; i++) {
			ret += q[i].size();
		}
	}

	void start() {
		e = new SchedEvent(this);
		e->sim = sim;
		e->start();
	}

	void clean_finished_fcfs(int i) {
		if (q[i].empty())
			return;

		Job *j = q[i].front();
		if (j->rem <= 0) {
			q[i].pop_front();
			Scheduler::finish(j);
			delete j;
		}
	}

	void clean_finished_ps(int i) {
		LET(it, q[i].begin());
		LET(curr, it);
		while (it != q[i].end()) {
			Job *j = *it;
			curr = it++;
			if (j->rem <= 0) {
				q[i].erase(curr);
				Scheduler::finish(j);
				delete j;
			}
		}
	}

	void clean_finished() {
		for (int i = 0; i < levels; i++) {
			if (i < fcfs_level)
				clean_finished_fcfs(i);
			else
				clean_finished_ps(i);
		}
	}

	/* Process jobs and return elapsed time */
	double process() {
		int i;
		double ret = 0.0;

		i = 0;
		while (i < levels) {
			if (q[i].size())
				break;
			i++;
		}

		if (i < fcfs_level) {
			ret = process_fcfs(i);
		} else if (i < levels) {
			ret = process_ps(i);
		}

		clean_finished();
		return ret;
	}

	double process_ps(int i) {
		int n = q[i].size();
		double work = get_work();

		if (work == 0)
			return capacity;

		assert(n);
		EACH(it, q[i]) {
			Job *j = *it;
			j->rem -= work / n;
		}

		return capacity;
	}

	double process_fcfs(int i) {
		double work = get_work();
		assert(q[i].size());

		if (work == 0)
			return capacity;

		Job *j = q[i].front();
		j->rem -= work;
		return capacity;
	}
};
