#include <queue>
#include <list>
#include <cassert>
#include <string>
using namespace std;

#define LET(it, x) typeof(x) it(x)
#define EACH(it, cont) for (LET(it, (cont).begin()); it != (cont).end(); ++it)
#define REP(i, n) for (int i = 0; i < n; ++i)

int opts_log = 0;
int opts_time = 10000000;
float opts_load = 0.5;
int opts_cutoff = 50;

#include <stdio.h>
#include "random.h"
#include "sim.h"
#include "job.h"
#include "sched.h"
#include "ps.h"
#include "srpt.h"
#include "fcfs.h"
#include "las.h"
#include "mlps.h"

void parse_args(int argc, char *argv[]) {
	int i = 0;
	string a[argc];
	for (i = 0; i < argc; i++)
		a[i] = string(argv[i]);

	i = 0;
	while (i < argc) {
		if (a[i] == "-l")
			opts_log = 1;

		if (a[i] == "-t")
			opts_time = atoi(a[i+1].c_str());

		if (a[i] == "-L")
			opts_load = atof(a[i+1].c_str());

		if (a[i] == "-c")
			opts_cutoff = atoi(a[i+1].c_str());

		i++;
	}
}

int main(int argc, char *argv[]) {
	rand_init();
	parse_args(argc, argv);

	double capacity = 1.0; /* size per second */
	double mean_size = 10.0; /* size */
	double load = opts_load; /* dimensionless */
	double mean_inter = mean_size / (load * capacity);

	Sim *sim = new Sim;
	// RandomExp *sizes = new RandomExp(mean_size);
	RandomPareto *sizes = new RandomPareto(mean_size, 1.1);
	RandomExp *inter = new RandomExp(mean_inter);
	Arrival *arr = new Arrival(sizes, inter);

	FCFSScheduler *fcfs = new FCFSScheduler(capacity);
	PSScheduler *ps = new PSScheduler(capacity);
	SRPTScheduler *srpt = new SRPTScheduler(capacity);
	LASScheduler *las = new LASScheduler(capacity);
	MLPSScheduler *mlps = new MLPSScheduler(capacity, 4, 2);

	mlps->set_cutoff(0, 0);
	mlps->set_cutoff(1, opts_cutoff);
	mlps->set_cutoff(2, 2*opts_cutoff);
	mlps->set_cutoff(3, 3*opts_cutoff);

	vector<Scheduler *> sch;
	sch.push_back(ps);
	sch.push_back(fcfs);
	sch.push_back(srpt);
	sch.push_back(las);
	sch.push_back(mlps);

	EACH(it, sch) {
		Scheduler *sched = *it;

		rand_init();
		sim->init();
		arr->sim = sim;
		arr->sched = sched;
		sched->sim = sim;

		arr->start();
		sched->start();
		sim->run(opts_time);
		sched->stats();
	}

	return 0;
}
