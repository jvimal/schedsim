struct Sim;
struct Scheduler;

struct Event {
	double t;
	Sim *sim;
	bool inq;
	int canceled;

	Event():inq(false), canceled(0) { }
	virtual void run() = 0;
	virtual const char *n() = 0;
};

struct EventPtrCompare: public std::binary_function<Event*, Event*, bool> {
	bool operator() (const Event *lhs, const Event *rhs) const {
		return lhs->t > rhs->t;
	}
};

struct Sim {
	priority_queue<Event*, vector<Event *>, EventPtrCompare> eq;
	double now;

	Sim():now(0) {}

	void init() {
		now = 0;
		while (!eq.empty()) {
			Event *e = eq.top(); eq.pop();
			e->inq = false;
		}
	}

	void schedule(Event *e, double t) {
		if (e->inq)
			return;
		e->t = now + t;
		e->sim = this;
		e->inq = true;
		eq.push(e);
	}

	void run(double T) {
		double t;
		while (!eq.empty()) {
			Event *e = eq.top(); eq.pop();
			if (e->t > T)
				break;

			assert(e->t >= now);
			now = e->t;
			e->inq = false;
			e->run();
		}

		if (opts_log)
			printf("[%.3lf] Simulation finishing\n", now);
	}
};
