
struct SRPTScheduler : public Scheduler {
	list<Job*> q;
	SchedEvent *e;
	list<Job*>::iterator first;

	SRPTScheduler(double capacity) {
		this->capacity = capacity;
		name = "SRPT";
	}

	void addjob(Job *j) {
		q.push_back(j);
		if (q.size() == 1) {
			last_process = sim->now;
			sim->schedule(e, 0);
		} else {
			process();
		}
	}

	int qlen() { return q.size(); }

	void start() {
		e = new SchedEvent(this);
		e->sim = sim;
		e->start();
	}

	list<Job*>::iterator find_smallest_and_clean_finished() {
		int smallest = INT_MAX;
		LET(it, q.begin());
		LET(curr, it);
		first = q.end();

		while (it != q.end()) {
			Job *j = *it;
			curr = it++;
			if (j->rem > 0 && j->rem < smallest) {
				smallest = j->rem;
				first = curr;
			}

			if (j->rem <= 0) {
				q.erase(curr);
				Scheduler::finish(j);
			}
		}

		return first;
	}

	/* Process jobs and return elapsed time */
	double process() {
		double work = get_work();
		if (work == 0)
			return capacity;

		LET(first, find_smallest_and_clean_finished());

		if (first != q.end()) {
			Job *j = *first;
			double consumed = min(j->rem, capacity);
			j->rem -= consumed;
			return capacity;
		}

		return capacity;
	}
};
