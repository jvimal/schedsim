
static int jid;
struct Job {
	int id;
	double size;
	double rem;
	double start;
	double end;

	Job(double size, double start):size(size), rem(size), start(start) {
		id = jid++;
	}

	double finish(double curr) {
		if (opts_log)
			printf("Job %d finished at time %.3f (elapsed %.3lf)\n", id, curr, curr - start);
		return curr - start;
	}
};

struct JobSizePtrCompare: public std::binary_function<Job*, Job*, bool> {
	bool operator() (const Job *lhs, const Job *rhs) const {
		return lhs->size > rhs->size;
	}
};

struct JobStartPtrCompare: public std::binary_function<Job*, Job*, bool> {
	bool operator() (const Job *lhs, const Job *rhs) const {
		return lhs->start > rhs->start;
	}
};

struct JobRemPtrCompare: public std::binary_function<Job*, Job*, bool> {
	bool operator() (const Job *lhs, const Job *rhs) const {
		return lhs->rem > rhs->rem;
	}
};

