
CPP=g++
FLAGS=-O3

all:
	$(CPP) $(FLAGS) sched.cpp -o sched
	$(CPP) $(FLAGS) rand.cpp -o rand
