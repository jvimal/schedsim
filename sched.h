
double avg(vector<double> &v) {
	double a = 0.0;
	EACH(it, v)
		a += *it;
	return a / v.size();
}

struct Scheduler {
	double capacity;
	Sim *sim;
	string name;
	vector<double> sjs, js;
	vector<double> sjs_conditioned, js_conditioned;
	double last_process;

	virtual void addjob(Job *j) = 0;
	virtual double process() = 0;
	virtual int qlen() = 0;
	virtual void start() = 0;

	Scheduler():last_process(0) { }

	double get_work() {
		double ret;
		if (sim->now == last_process)
			return 0;

		ret = capacity * (sim->now - last_process);
		last_process = sim->now;
		return ret;
	}

	void finish(Job *j) {
		double f = j->finish(sim->now);
		sjs.push_back(f);
		js.push_back(j->size);

		if (j->size <= opts_cutoff) {
			sjs_conditioned.push_back(f);
			js_conditioned.push_back(j->size);
		}
	}

	void stats() {
		stats_all(sjs, js, (char *)"Everything");
		stats_all(sjs_conditioned, js_conditioned, (char *)"Conditioned");
		printf("\n\n");
	}

	void stats_all(vector<double> &sjs, vector<double> &js, char *msg) {
		double a, b;
		int n = sjs.size();
		sort(sjs.begin(), sjs.end());
		a = avg(sjs);

		printf("---- %s Stats (%s,%.3lf) ----\n", name.c_str(), msg, avg(js));
		if (!n) {
			printf("Error: no jobs completed\n");
			exit(-1);
		}


		printf("   n:    %10d\n"
		       " Avg:    %10.3lf\n"
		       "90th:    %10.3lf\n"
		       "99th:    %10.3lf\n"
		       "99.9th:  %10.3lf\n"
		       "99.99th: %10.3lf\n"
		       "max:     %10.3lf\n",
		       n, a,
		       sjs[int(0.9 * n)],
		       sjs[int(0.99 * n)],
		       sjs[int(0.999 * n)],
		       sjs[int(0.9999 * n)],
		       sjs.back());
	}
};


struct SchedEvent : public Event {
	Sim *sim;
	Scheduler *sched;
	static const string name;

	SchedEvent(Scheduler *sched):sched(sched) {}

	const char *n() { return name.c_str(); }

	void start() {
		inq = false;
		sim->schedule(this, 0);
	}

	void run() {
		double elapsed;
		elapsed = sched->process();

		if (elapsed > 0) {
			sim->schedule(this, elapsed);
		}
	}
};

const string SchedEvent::name = "SchedEvent";


struct Arrival : public Event {
	Random *sizes;
	Random *inter;
	Sim *sim;
	Scheduler *sched;
	static const string name;

	Arrival(Random *sizes, Random *inter):sizes(sizes), inter(inter) {}

	const char *n() { return name.c_str(); }

	void start() {
		inq = false;
		sim->schedule(this, 0);
	}

	void run() {
		Job *j = new Job(sizes->gen(), sim->now);

		if (opts_log)
			printf("[%.3lf] Adding job %d size %.3lf\n", sim->now, j->id, j->size);
		sched->addjob(j);
		sim->schedule(this, inter->gen());
	}
};
const string Arrival::name = "Arrival";

