
import random

class Process(object):
    def __init__(self, name):
        self.name = name

class PoissonProcess(Process):
    def __init__(self, rate=1.0):
        Process.__init__(self, name="PoissonProces[rate=%.3f]" % rate)
        self.r = lambda: random.expovariate(1.0/rate)

    def get(self):
        return self.r()

class DeterministicProcess(Process):
    def __init__(self, rate=1.0):
        Process.__init__(self, name="DeterministicProcess[rate=%.3f]" % rate)
        self.r = lambda: rate

    def get(self):
        return self.r()

class Queue(object):
    def __init__(self, name, arrival, outq=None):
        self.q = 0.0
        self.name = name
        self.arrival = arrival
        self.outq = outq

    def empty(self):
        return self.q == 0

    def run(self):
        self.enqueue()

    def enqueue(self):
        if self.arrival:
            self.enqueue_amt(self.arrival.get())

    def enqueue_amt(self, amt):
        self.q += amt

    def dequeue(self, amt):
        consumed = min(self.q, amt)
        self.q -= consumed
        if self.outq:
            self.outq.enqueue_amt(consumed)
        return consumed

    def prn(self):
        print "Queue[%s] occ %.3f" % (self.name, self.q)

class Scheduler(object):
    def __init__(self, rate, queues=[]):
        self.rate = rate
        self.queues = queues
        self.nqueues = len(queues)

    def run(self):
        pass

class DRRScheduler(Scheduler):
    def __init__(self, rate, queues=[]):
        Scheduler.__init__(self, rate, queues)
        self.qi = 0
        self.deficit = [self.rate] * len(queues)

    def next(self):
        count = 0
        while count < self.nqueues:
            self.qi += 1
            count += 1
            if self.qi >= self.nqueues:
                self.qi = 0
            if not self.queues[self.qi].empty():
                break
        return self.qi

    def run(self):
        total = self.rate
        consumed = 0
        while 1:
            self.next()
            #self.deficit[self.qi] += self.rate
            #self.deficit[self.qi] = min(self.rate, self.deficit[self.qi])
            consumed = self.queues[self.qi].dequeue(total)
            total -= consumed
            # print "Sched: servicing %d next, total = %.3f, consumed = %.3f, rate = %.3f" % (self.qi, total, consumed, self.rate)
            if consumed == 0 or total == 0:
                break

class PriorityScheduler(Scheduler):
    def run(self):
        total = self.rate
        consumed = 0
        for q in self.queues:
            consumed = q.dequeue(total)
            total -= consumed
            if total == 0:
                break

class Simulator(object):
    def __init__(self, queues, schedulers):
        self.t = 0.0
        self.dt = 1.0
        self.queues = queues
        self.schedulers = schedulers

    def run(self, iter=10):
        while iter:
            iter -= 1
            self.run_once()
        return

    def run_once(self):
        for q in self.queues:
            q.run()

        for s in self.schedulers:
            s.run()
        self.t += 1

    def prn(self):
        for q in self.queues:
            q.prn()

def main():
    queues = []
    schedulers = []

    # Create the output queues first
    q20 = Queue(name="From00", arrival=None)
    s2 = DRRScheduler(rate=1, queues=[q20])

    q30 = Queue(name="From01", arrival=None)
    q31 = Queue(name="From10", arrival=None)
    s3 = PriorityScheduler(rate=1, queues=[q31, q30])

    # Input queues
    q00 = Queue(name="Poisson00", arrival=PoissonProcess(rate=0.6), outq=q20)
    q01 = Queue(name="Poisson01", arrival=PoissonProcess(rate=0.33), outq=q30)
    #s0 = DRRScheduler(rate=1, queues=[q00, q01])
    s0 = PriorityScheduler(rate=1, queues=[q00, q01])

    q10 = Queue(name="Poisson10", arrival=PoissonProcess(rate=0.6), outq=q31)
    s1 = DRRScheduler(rate=1, queues=[q10])

    # Simple example
    #queues.append(Queue(name="Poisson", arrival=PoissonProcess(rate=0.8)))
    #queues.append(Queue(name="Determ", arrival=DeterministicProcess(rate=0.4)))
    #schedulers.append(DRRScheduler(rate=1, queues=queues))
    #schedulers.append(PriorityScheduler(rate=1, queues=queues))

    queues = [q00, q01, q10, q20, q30, q31]
    schedulers = [s0, s1, s2, s3]
    sim = Simulator(queues=queues, schedulers=schedulers)
    sim.run(iter=10000)
    sim.prn()

if __name__ == "__main__":
    main()
