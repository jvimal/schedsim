
struct PSScheduler : public Scheduler {
	list<Job*> q;
	SchedEvent *e;

	PSScheduler(double capacity) {
		this->capacity = capacity;
		name = "PS";
	}

	void addjob(Job *j) {
		q.push_back(j);
		if (q.size() == 1) {
			last_process = sim->now;
			sim->schedule(e, 0);
		} else {
			process();
		}
	}

	int qlen() { return q.size(); }

	void start() {
		e = new SchedEvent(this);
		e->sim = sim;
		e->start();
	}

	void clean_finished() {
		LET(it, q.begin());
		LET(curr, it);
		while (it != q.end()) {
			Job *j = *it;
			curr = it++;
			if (j->rem <= 0) {
				q.erase(curr);
				Scheduler::finish(j);
				delete j;
			}
		}
	}

	/* Process jobs and return elapsed time */
	double process() {
		int n = q.size();
		if (n == 0)
			return capacity;

		double work = get_work();
		if (work == 0)
			return capacity;

		assert(n);
		EACH(it, q) {
			Job *j = *it;
			j->rem -= work / n;
		}

		clean_finished();
		return capacity;
	}
};
